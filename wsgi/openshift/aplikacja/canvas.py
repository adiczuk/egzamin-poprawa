#!/usr/bin/env python
import requests
import html5lib
from bs4 import BeautifulSoup as bs
import memoryZip
import redis
from rq.decorators import job
from memcache import RedisCache

conn = redis.StrictRedis(host='pub-redis-10183.us-east-1-3.2.ec2.garantiadata.com', port=10183, db=0, password="baza")

mc = RedisCache(conn)

url = {
    'login' : 'https://canvas.instructure.com/login',
    'courses' : 'https://canvas.instructure.com/courses',
}

def logincanvas(username, password):
    payload = {
        'pseudonym_session[unique_id]': username,
        'pseudonym_session[password]': password
        }
    session = requests.session()
    session.post(url["login"], data=payload)
    return session

def get_courses(username, password):
    session = logincanvas(username, password)

    request = session.get(url["courses"])
    html = request.text
    parsed1 = bs(html, "html5lib")

    przedmioty = []
    ids = []

    try:
        myuls = parsed1.find_all('ul', {'class' : "menu-item-drop-column-list"})
        ul = myuls[0]

        lis = ul.find_all('li', {'class' : 'customListItem'})
        for li in lis:
            ids.append(li["data-id"])

        spans = ul.find_all('span', {'class' : 'name ellipsis'})

        i = 0
        for span in spans:
            przedmioty.append([span.findAll(text=True)[0], ids[i]])
            i+=1

    except:
        return None

    return przedmioty

def get_all_grades(username, password, course_id):
    session = logincanvas(username, password)

    request = session.get('https://canvas.instructure.com/courses/'+course_id+'/grades')
    html = request.text
    parsed1 = bs(html, "html5lib")

    types = []
    ids = dict()
    i = 0

    for typ in parsed1.find_all('tr', {'class' : 'student_assignment  hard_coded group_total'}):
        types.append([])
        ids[typ.findAll(text=True)[1].strip()] = i
        i+=1

    for x in parsed1.find_all('tr', {'class' : 'student_assignment assignment_graded editable'}):
        typ = x.find('div', {'class' : 'context'})
        id = ids[typ.findAll(text=True)[0]]

        name = x.find('th', {'class' : 'title'}).a.findAll(text=True)[0]
        date = x.find('td', {'class' : 'due'}).findAll(text=True)[0].strip()
        score = x.find('span', {'class' : 'score'}).findAll(text=True)[0].split()[0]

        types[id].append([name, date, score])

    for rodzaj in types:
        rodzaj.sort(key=lambda x: x[1])

    return ids, types

@job('check', connection=conn)
def check_cache(user, key):
    return mc.get('zip_' + str(key) + '_' + str(user))

@job('zip', connection=conn, timeout=100)
def download_zip(user, key, ids, tablice):

    imz = memoryZip.InMemoryZip()

    for id in ids:
        txt = ""
        for nazwa, data, wynik in tablice[ids[id]]:
            txt += nazwa+ " Punkty: "+wynik+" Data: "+ data + "\n"
        imz.append(id+'.txt', txt.encode('utf-8'))

    read = imz.read()

    mc.set('zip_' + str(key) + '_' + str(user), read, timeout=300)

    return read