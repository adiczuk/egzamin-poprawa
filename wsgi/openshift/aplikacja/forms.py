# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, HiddenInput, Textarea, PasswordInput
from models import UserCanvas

class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required = True)

    class Meta:
        model = User
        fields = [
            'username', 'email', 'password1', 'password2'
        ]

        def save(self, commit = True):
            user = super(MyRegistrationForm, self).save(commit = False)
            user.email = self.cleaned_data['email']

            if commit:
                user.save()

            return user

    def clean_email(self):
        email = self.cleaned_data['email']
        users = User.objects.all()
        for user in users:
            if email == user.email:
                raise forms.ValidationError(u"Podany adres e-mail jest już używany!")
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        dl = len(username)
        if dl < 3:
            raise forms.ValidationError(u"Login musi mieć co najmniej 3 znaki.")
        return username

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        dl = len(password1)
        if dl < 4:
            raise forms.ValidationError(u"Hasło musi mieć co najmniej 4 znaki.")
        return password1

class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'password'
        ]
        widgets = {
            'password' : PasswordInput(),
        }

class UstawForm(ModelForm):
    class Meta:
        model = UserCanvas
        fields = [
            'login', 'password'
        ]
        widgets = {
            'password' : PasswordInput(),
        }

        def save(self, commit = True):
            user = super(UstawForm, self).save(commit = False)

            if commit:
                user.save()

            return user