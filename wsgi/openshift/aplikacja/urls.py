from django.conf.urls import patterns, url
from django.views.generic import ListView
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.loginn, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^email/(?P<key>.+)$', views.potwierdzenie, name='email'),
    url(r'^grades/$', views.grades, name='grades'),
    url(r'^download/(?P<key>.+)$', views.download, name='download'),
    url(r'^ustaw/$', views.ustaw, name='ustaw'),
    url(r'^logout/$', views.wyloguj, name='logout'),
    url(r'^sprawdz/$', views.sprawdz, name='sprawdz'),
    url(r'^dalej/$', views.dalej, name='dalej'),
    url(r'^zabij/$', views.zabij, name='zabij'),
)