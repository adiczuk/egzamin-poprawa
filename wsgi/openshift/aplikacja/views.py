# -*- coding: utf-8 -*-
import datetime
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from forms import MyRegistrationForm, LoginForm,UstawForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from uuid import uuid4 as uuid
from models import UserKey, UserCanvas
from django.contrib.auth.decorators import login_required
import canvas

def index(request):
    info = request.session.get('info', None)
    if info:
        del request.session['info']
    return render(request, "aplikacja/home.html", {"info" : info})

def register(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            WyslijMail(request.POST['username'])
            request.session['info'] = u'Twoje konto zostało zarejestrowane! Wejdź na swoją skrzynkę mailową, aby potwierdzić rejestrację!'
            return HttpResponseRedirect(reverse('app:index'))
    else:
        form = MyRegistrationForm()
    return render(request, "aplikacja/register.html", {'form': form})

def loginn(request):
    info = request.session.get('info', None)

    if request.method == 'GET':
        if request.user.is_authenticated():
            request.session['info'] = u'Jesteś już zalogowany!'
            return HttpResponseRedirect(reverse('app:index'))
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'aplikacja/login.html', {"info" : info, 'form' : form})

    elif request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])

        try:
            is_activte = UserKey.objects.get(user=user).is_active
        except:
            is_activte = False

        if user and is_activte:
            login(request, user)
            request.session['info'] = u'Zostałeś pomyślnie zalogowany!'
            return HttpResponseRedirect(reverse('app:index'))
        else:
            info = u'Złe dane!'
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'aplikacja/login.html', {"info" : info, 'form' : form})

def wyloguj(request):
    logout(request)
    request.session['info'] = u'Zostałeś wylogowany!'
    return HttpResponseRedirect(reverse('app:index'))

def potwierdzenie(request, key):
    uk = UserKey.objects.get(key=key)
    uk.is_active = True
    uk.save()
    request.session['info'] = u'Potwierdzenie przebiegło pomyślnie! Teraz możesz się zalogować'
    return HttpResponseRedirect(reverse('app:index'))

def WyslijMail(username):
    user = User.objects.get(username=username)
    key = uuid().hex
    uk = UserKey(key=key, user=user, is_active=False)
    uk.save()

    uc = UserCanvas(user=user, login="", password="", is_set=False)
    uc.save()

    od = 'microblog@django.pl'
    do = user.email #'adiczuk@gmail.com'

    subject = 'Potwierdzenie rejestracji'
    message = 'Dziekujemy za rejestacje na naszym microblogu '+ username +'!\n' \
    'Aby dokonczyc rejestracje prosimy o wejscie w ponizszy link:\n' \
    'Link : ' + 'http://poprawa-adiczuk.rhcloud.com/app/email/' + key # 127.0.0.1:8000

    send_mail(subject, message, od, [do])

@login_required(login_url='/app/login')
def ustaw(request):
    if request.method == 'GET':
        form = UstawForm()
        return render(request, 'aplikacja/ustaw.html', {'form' : form})
    else:
        form = UstawForm(request.POST)
        x = form.save(commit=False)
        login = x.login
        password = x.password
        uc = UserCanvas.objects.get(user=request.user)
        uc.login = login
        uc.password = password
        uc.is_set = True
        uc.save()
        return HttpResponseRedirect(reverse('app:index'))

def grades(request):
    uc = GetCanvasUser(request)
    if uc and uc.is_set:
        courses = canvas.get_courses(uc.login, uc.password)
        return render(request, 'aplikacja/oceny.html', {'kursy' : courses})
    else:
        return HttpResponseRedirect(reverse('app:ustaw'))

def download(request, key):
    if request.session.get('login', None):
        zip = canvas.check_cache(request.session['login'], key)
        if zip is None:
            ids, tablice = canvas.get_all_grades(request.session['login'], request.session['password'], key)
            zip = canvas.download_zip(request.session['login'], key, ids, tablice)
        response = HttpResponse(zip, content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=oceny_' + str(key) + '.zip'
        return response
    else:
        return HttpResponseRedirect(reverse('app:sprawdz'))

def GetCanvasUser(request):
    try:
        uc = UserCanvas.objects.get(user=request.user)
    except:
        uc = None
    return uc

def sprawdz(request):
    if request.method == 'GET' and request.session.get('login', None) is None:
        form = UstawForm()
        request.session.clear()
        return render(request, 'aplikacja/ustaw2.html', {'form' : form})
    elif (request.session.get('login', None) and request.session.get('password', None)) or request.method == 'POST':
        if request.method == 'POST':
            request.session.clear()
            form = UstawForm(request.POST)
            x = form.save(commit=False)
            login = x.login
            password = x.password
            request.session['login'] = login
            request.session['password'] = password
            request.session.set_expiry(120)
        courses = canvas.get_courses(request.session['login'], request.session['password'])
        if courses is None:
            request.session.clear()

        czas = 120 - int(request.session.get_expiry_date().second)
        return render(request, 'aplikacja/oceny.html', {'kursy' : courses, 'czas' : czas})

def dalej(request):
    if request.session.get('login', None):
        request.session.set_expiry(120)
    return HttpResponseRedirect(reverse('app:sprawdz'))

def zabij(request):
    request.session.clear()
    return HttpResponseRedirect(reverse('app:sprawdz'))
