from django.db import models
from django.contrib.auth.models import User

class UserCanvas(models.Model):
    user = models.OneToOneField(User)
    is_set = models.BooleanField()
    login = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

class UserKey(models.Model):
    key = models.CharField(max_length=50)
    user = models.OneToOneField(User)
    is_active = models.BooleanField()